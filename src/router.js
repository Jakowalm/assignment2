import Vue from 'vue'
import VueRouter from 'vue-router'
import StartPage from './components/StartPage.vue'
import QuestionPage from './components/QuestionPage.vue'
import ResultsPage from './components/ResultsPage.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/start',
        alias: '/',
        component: StartPage
    },
    {
        path: '/questions',
        component: QuestionPage
    },
    {
        path: '/results',
        component: ResultsPage 
    }
]

export default new VueRouter({ routes })
