
export function getQuestions    (questionAmount,categorySelect,questionDifficulty) {
    let tail = "amount="+questionAmount;
    if (categorySelect > 0) {
        tail += "&category="+categorySelect;
    }
    if (questionDifficulty) {
        tail += "&difficulty="+questionDifficulty;
    }
    return fetch("https://opentdb.com/api.php?"+tail)
      .then(response => {
          if (response.status !== 200) {
              throw new Error('Couldn\'t fetch questions :(');
          }
          return response.json();
      })
      .then(result => {
          return result.results;
      })
  }